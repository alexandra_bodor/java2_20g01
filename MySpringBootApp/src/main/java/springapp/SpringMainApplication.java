package springapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMainApplication {
	//http://localhost:8080/
	public static void main(String[] args) {
		SpringApplication.run(SpringMainApplication.class, args);
	}

}
